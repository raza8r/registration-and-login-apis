
const router = require('express').Router();
const CoursesType = require('../model/Courses');

//Register Course Type
router.post('/coursetype', async (req, res) => {
    try {
        const courseType = new CoursesType({
            name: req.body.name
        });
        const savedCourse = await courseType.save();
        res.status(200).json({ savedCourse });
    }
    catch (err) {
        res.status(400).json({ message: err });
    }
});


// //Get parents Record
// router.get('/students', async (req, res) => {
//     try {
//         const studentRecord = await Students.find();
//         res.status(200).json(studentRecord);
//     }
//     catch (err) {
//         res.json({ message: err });
//     }
// });

// //Search Specific Record
// router.get('/:studentsId', async (req, res) => {
//     try {
//         const student = await Students.findById(req.params.studentsId);
//         res.json(student);
//     } catch (err) {
//         res.json({ message: err });
//     }
// });

// //Update parents Record
// router.patch('/:studentsId', async (req, res) => {
//     try {
//         const studentUpdate = await Students.updateOne({ _id: req.params.studentsId }, {
//             $set: {
//                 name: req.body.name,
//                 date: req.body.date,
//                 classes: req.body.classes,
//                 city: req.body.city,
//                 state: req.body.state,
//                 gender: req.body.gender,
//                 age: req.body.age,
//                 parent: req.body.parent,
//                 username: req.body.username,
//                 // password: hashedPassword
//             }
//         });
//         res.status(200).json(studentUpdate)
//     }
//     catch (err) {
//         res.status(400).json({ message: err });
//     }
// });

// //Delete Teacher
// router.delete('/:studentsId', async (req, res) => {
//     try {
//         const studentsDelete = await Students.remove({ _id: req.params.studentsId });
//         res.status(200).json(studentsDelete);

//     } catch (err) {
//         res.status(400).json({ message: err });
//     }
// });

module.exports = router;