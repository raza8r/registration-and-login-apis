const router = require('express').Router();
const Parents = require('../model/Parents');
const { parentValidation, parentValidationLogin } = require('../validation');
const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');


//Register New Parents
router.post('/parents', async (req, res) => {

    //Validate Parents
    const { error } = parentValidation(req.body);
    if (error) return res.status(401).json({ error: error.details[0].message });

    //Email,  username and contact already exist
    const emailExist = await Parents.findOne({ email: req.body.email });
    if (emailExist) return res.status(401).json({ email: "Email already exist" });
    const userExist = await Parents.findOne({ username: req.body.username });
    if (userExist) return res.status(401).json({ error: "User is already exist" });
    const contact = await Parents.findOne({ contact: req.body.contact });
    if (contact) return res.status(401).json({ error: "Contact is already exist" });
    const skypeName = await Parents.findOne({ skypeName: req.body.skypeName });
    if (skypeName) return res.status(401).json({ message: "Skype name is already exist" });

    // Hash Password
    const salt = await bcrypt.genSalt(10);
    const hashedPassword = await bcrypt.hash(req.body.password, salt);

    try {
        const parent = new Parents({
            name: req.body.name,
            email: req.body.email,
            username: req.body.username,
            password: hashedPassword,
            contact: req.body.contact,
            skypeName: req.body.skypeName
        });
        const savedParent = await parent.save();
        //pass single id not complete data
        // res.status(200).json({teacher: teacher._id});

        res.status(200).json({ savedParent });
    }
    catch (err) {
        res.status(400).json({ message: err });
    }
});

//Login Parent
router.post('/parentlogin', async (req, res) => {

    //Validation
    const { error } = await parentValidationLogin(req.body);
    if (error) return res.status(400).json({ error: error.details[0].message });

    //Check if the username exist
    const parent = await Parents.findOne({ username: req.body.username });
    if (!parent) return res.status(400).json({ error: "Username is not exist" });

    //compare the password
    const loginPassword = await bcrypt.compare(req.body.password, parent.password);
    if (!loginPassword) return res.status(400).json({ error: "Invalid Password" });

    //create and assign token
    const token = jwt.sign({ _id: parent._id }, process.env.TOKEN_SECRET);
    res.header('auth-token', token).send(token);

    // res.json("Login");
});

//Get parents Record
router.get('/parents', async (req, res) => {
    try {
        const parnentRecord = await Parents.find();
        res.status(200).json(parnentRecord);
    }
    catch (err) {
        res.json({ message: err });
    }
});

//Search Specific Record
router.get('/:parentsId', async (req, res) => {
    try {
        const parent = await Parents.findById(req.params.parentsId);
        res.json(parent);
    } catch (err) {
        res.json({ message: err });
    }
});

//Update parents Record
router.patch('/:parentsId', async (req, res) => {
    try {
        const parentUpdate = await Parents.updateOne({ _id: req.params.parentsId }, {
            $set: {
                name: req.body.name,
                email: req.body.email,
                username: req.body.username,
                password: req.body.password,
                contact: req.body.contact,
                skypeName: req.body.skypeName
            }
        });
        res.status(200).json(parentUpdate)
    }
    catch (err) {
        res.status(400).json({ message: err });
    }
});

//Delete Teacher
router.delete('/:parentsId', async (req, res) => {
    try {
        const parentDelete = await Parents.remove({ _id: req.params.parentsId });
        res.status(200).json(parentDelete);

    } catch (err) {
        res.status(400).json({ message: err });
    }
});

module.exports = router;