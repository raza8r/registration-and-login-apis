const router = require('express').Router();
const verify = require('./verifyToken');

router.get('/posts', verify, (req,res)=>{
    res.send('This is authorized posts page');
});

module.exports = router;
