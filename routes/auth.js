const router = require('express').Router();
const User = require('../model/User');
const { registerValidaton, loginValidaton } = require('../validation');
const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');
// require('dotenv/config');

//Register new user
router.post('/register', async (req, res) => {

    //Validate registration
    const { error } = registerValidaton(req.body);
    if (error) return res.status(400).json({error: error.details[0].message});

    //Validate the email
    const emailExist = await User.findOne({ email: req.body.email });
    if (emailExist) return res.status(400).json({error: "Email already exists"});

    //Hash Password
    const salt = await bcrypt.genSalt(10);
    const hashedPassword = await bcrypt.hash(req.body.password, salt);

    try {
        const user = new User({
            name: req.body.name,
            email: req.body.email,
            password: hashedPassword
        });

        const savedData = await user.save();
        // res.send({ user: user._id });
        res.status(200).json({user: user._id});
    }
    catch (err) {
        res.status(400).json({message: "Error"});
    }
});

//Login user
router.post('/login', async (req, res) => {
    //validation
    const { error } = await loginValidaton(req.body);
    if (error) return res.status(400).send(error.details[0].message)

    //Check if email exist in database
    const user = await User.findOne({ email: req.body.email });
    if (!user) return res.status(400).send('Email is not exist');

    //Check if the password is match or not
    const loginPass = await bcrypt.compare(req.body.password, user.password);
    if (!loginPass) return res.status(400).send("Invalid Password");

    //Create and assign a token
    const token = jwt.sign({ _id: user._id }, process.env.TOKEN_SECRET);
    res.header('auth-token', token).send(token);

    // res.json('LogdIn');

});

module.exports = router;