const Joi = require('@hapi/joi');

const registerValidaton = (data) => {
    const schema = Joi.object({
        name: Joi.string().min(4).required(),
        email: Joi.string().min(6).required().email(),
        password: Joi.string().min(6).required()
    });
    return schema.validate(data);
};

const loginValidaton = (data) => {
    const schema = Joi.object({
        email: Joi.string().min(6).required().email(),
        password: Joi.string().min(6).required()
    });
    return schema.validate(data);
};

const teacherValidation = (data) => {
    const schema = Joi.object({
        name: Joi.string().min(4).required(),
        cnic: Joi.string().min(4).required(),
        gender: Joi.string().min(4).required(),
        degree: Joi.string().min(4).required(),
        salaryType: Joi.string().min(4).required(),
        salary: Joi.string().min(4).required(),
        username: Joi.string().min(4).required(),
        password: Joi.string().min(4).required()

    });
    return schema.validate(data);
};


//Parent Validations

const parentValidation = (data) => {
    const schema = Joi.object({
        name: Joi.string().required().min(4),
        email: Joi.string().required().min(4).email(),
        username: Joi.string().required().min(4),
        password: Joi.string().required().min(4),
        contact: Joi.string().required().min(4),
        skypeName: Joi.string().required().min(4),
    });
    return schema.validate(data)
}

const parentValidationLogin = (data) => {
    const schema = Joi.object({
        username: Joi.string().required(),
        password: Joi.string().required()
    });
    return schema.validate(data);
}

//Student Validations

const studentValidation = (data) => {
    const schema = Joi.object({
        name: Joi.string().required().min(3),
        classes: Joi.string().required(),
        city: Joi.string().required(),
        state: Joi.string().required(),
        gender: Joi.string().required(),
        age: Joi.string().required(),
        parent: Joi.string().required(),
        username: Joi.string().required(),
        password: Joi.string().required()
    });
    return schema.validate(data);
}

const studentValidationLogin = (data) => {
    const schema = Joi.object({
        username: Joi.string().required(),
        password: Joi.string().required()
    });
    return schema.validate(data);
}

module.exports.registerValidaton = registerValidaton;
module.exports.loginValidaton = loginValidaton;
module.exports.teacherValidation = teacherValidation;
module.exports.parentValidation = parentValidation;
module.exports.parentValidationLogin = parentValidationLogin;
module.exports.studentValidation = studentValidation;
module.exports.studentValidationLogin = studentValidationLogin;
