
const router = require('express').Router();
const AssignCourses = require('../model/AssignCourses');

//Assign Courses
router.post('/assigns', async (req, res) => {
    try {
        const assign = new AssignCourses({
            teacher: req.body.teacher,
            course: req.body.course
        });
        const savedAssign = await assign.save();
        res.status(200).json({ savedAssign });
    }
    catch (err) {
        res.status(400).json({ message: err });
    }
});

//Get Assign Courses

router.get('/assigns', async (req, res) => {
    try {
        const assign = await AssignCourses.find();
        res.status(200).json(assign);
    }
    catch (err) {
        res.json({ message: err });
    }
});

//Search Specific Record
router.get('/:assignId', async (req, res) => {
    try {
        const assign = await AssignCourses.findById(req.params.assignId);
        res.status(200).json(assign);
    } catch (err) {
        res.status(400).json({ message: err });
    }
});

//Update Assign Courses
router.patch('/:assignId', async (req, res) => {
    try {
        const assign = await AssignCourses.updateOne({ _id: req.params.assignId }, {
            $set: {
                teacher: req.body.teacher,
                course: req.body.course
            }
        });
        res.status(200).json(assign)
    }
    catch (err) {
        res.status(400).json({ message: err });
    }
});

//Delete Teacher
router.delete('/:assignId', async (req, res) => {
    try {
        const assign = await AssignCourses.remove({ _id: req.params.assignId });
        res.status(200).json(assign);

    } catch (err) {
        res.status(400).json({ message: err });
    }
});

module.exports = router;