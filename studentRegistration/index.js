const router = require('express').Router();
const Students = require('../model/Students');
const { studentValidation, studentValidationLogin } = require('../validation');
const jwt = require('jsonwebtoken');
const bcrypt = require('bcryptjs');


//Register New Parents
router.post('/students', async (req, res) => {

    //Validate Students
    const { error } = studentValidation(req.body);
    if (error) return res.status(200).json({ error: error.details[0].message });
    //Username already exist
    const userExist = await Students.findOne({ username: req.body.username });
    if (userExist) return res.status(401).json({ username: "This username is already exist" });
    // Hash Password
    const salt = await bcrypt.genSalt(10);
    const hashedPassword = await bcrypt.hash(req.body.password, salt);

    try {
        const student = new Students({
            name: req.body.name,
            date: req.body.date,
            classes: req.body.classes,
            city: req.body.city,
            state: req.body.state,
            gender: req.body.gender,
            age: req.body.age,
            parent: req.body.parent,
            username: req.body.username,
            password: hashedPassword
        });
        const savedStudent = await student.save();
        //pass single id not complete data
        // res.status(200).json({teacher: teacher._id});

        // Students.find().populate().then(students => {
        //         console.log('Students are', JSON.stringify(students, null, 4))
        //     })
        res.status(200).json({ savedStudent });
    }
    catch (err) {
        res.status(400).json({ message: err });
    }
});

//Login Parent
router.post('/studentlogin', async (req, res) => {

    //Validation
    const { error } = await studentValidationLogin(req.body);
    if (error) return res.status(400).json({ error: error.details[0].message });

    //Check if the username not exist
    const student = await Students.findOne({ username: req.body.username });
    if (!student) return res.status(400).json({ username: "Username is not exist" });

    //compare the password
    const loginPassword = await bcrypt.compare(req.body.password, student.password);
    if (!loginPassword) return res.status(400).json({ error: "Invalid Password" });

    //create and assign token
    const token = jwt.sign({ _id: student._id }, process.env.TOKEN_SECRET);
    res.header('auth-token', token).json(token);

    // res.json("Login");
});

//Get parents Record
router.get('/students', async (req, res) => {
    try {
        const studentRecord = await Students.find();
        res.status(200).json(studentRecord);
    }
    catch (err) {
        res.json({ message: err });
    }
});

//Search Specific Record
router.get('/:studentsId', async (req, res) => {
    try {
        const student = await Students.findById(req.params.studentsId);
        res.json(student);
    } catch (err) {
        res.json({ message: err });
    }
});

//Update parents Record
router.patch('/:studentsId', async (req, res) => {
    try {
        const studentUpdate = await Students.updateOne({ _id: req.params.studentsId }, {
            $set: {
                name: req.body.name,
                date: req.body.date,
                classes: req.body.classes,
                city: req.body.city,
                state: req.body.state,
                gender: req.body.gender,
                age: req.body.age,
                parent: req.body.parent,
                username: req.body.username,
                // password: hashedPassword
            }
        });
        res.status(200).json(studentUpdate)
    }
    catch (err) {
        res.status(400).json({ message: err });
    }
});

//Delete Teacher
router.delete('/:studentsId', async (req, res) => {
    try {
        const studentsDelete = await Students.remove({ _id: req.params.studentsId });
        res.status(200).json(studentsDelete);

    } catch (err) {
        res.status(400).json({ message: err });
    }
});

module.exports = router;