const express = require('express');
const app = express();
const mongoose = require('mongoose');
const authRoute = require('./routes/auth');
require('dotenv/config');
const bodyParser = require('body-parser');
const posts = require('./routes/post');
const teachers = require('./teachers/index');
const parent = require('./parentRegister/index');
const student = require('./studentRegistration/index');
const course = require('./registerCourse/index');
const courseType = require('./courseType/index');
const assign = require('./assignCourse/index');
const material = require('./courseMaterial/index');
var cors = require('cors');


app.use(cors());

//Middleware for body parser
app.use(bodyParser.json());

//Create Middleware for Route
app.use('/api/user', authRoute);
app.use('/api/user', posts);
app.use('/api/user', teachers);
app.use('/api/parent', parent);
app.use('/api/student', student);
app.use('/api/course', course);
app.use('/api/course', courseType);
app.use('/api/assign', assign);
app.use(express.static('uploads'));
app.use('/api/material', material);

// app.use('/api/user', records);

app.get('/home', (req, res) => {
    res.send("Home is here");
})

// Create Database connection
mongoose.connect(process.env.DB_CONNECTION, { useNewUrlParser: true, useUnifiedTopology: true }, () => {
    console.log("Database Connected")
});

//Cearte Server
app.listen('3001', () => {
    console.log('server is running')
});