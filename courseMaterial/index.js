
const router = require('express').Router();
const CourseMaterial = require('../model/CourseMaterial');
const multer = require('multer');

//Upload file
const storage = multer.diskStorage({
    destination: function (req, file, cb) {
        cb(null, './uploads/');
    },
    filename: function (req, file, cb) {
        cb(null, file.originalname);
    }
});
const upload = multer({ storage: storage });

//Register New Parents
router.post('/coursematerial', upload.single('courseFile'), async (req, res) => {
    // console.log(req.file)
    try {
        const material = new CourseMaterial({
            lesson: req.body.lesson,
            course: req.body.course,
            courseFile: req.file.path
        });
        const savedMaterial = await material.save();
        res.status(200).json({ savedMaterial });
    }
    catch (err) {
        res.status(400).json({ message: err });
    }
});

//Get Course Material Record

router.get('/coursematerial', async (req, res) => {
    try {
        const material = await CourseMaterial.find();
        res.status(200).json(material);
    }
    catch (err) {
        res.json({ message: err });
    }
});

//Search Specific Record
router.get('/:materialId', async (req, res) => {
    try {
        const material = await CourseMaterial.findById(req.params.materialId);
        res.json(material);
    } catch (err) {
        res.json({ message: err });
    }
});

//Update parents Record
router.patch('/:materialId', async (req, res) => {
    try {
        const material = await CourseMaterial.updateOne({ _id: req.params.materialId }, {
            $set: {
                lesson: req.body.lesson,
                course: req.body.course,
                courseFile: req.file.path

            }
        });
        res.status(200).json(material)
    }
    catch (err) {
        res.status(400).json({ message: err });
    }
});

//Delete Teacher
router.delete('/:materialId', async (req, res) => {
    try {
        const material = await CourseMaterial.remove({ _id: req.params.materialId });
        res.status(200).json(material);

    } catch (err) {
        res.status(400).json({ message: err });
    }
});

module.exports = router;