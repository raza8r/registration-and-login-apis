const mongoose = require('mongoose');

const parentSchema = mongoose.Schema({
    name: {
        type: String,
        required: true,
        min: 4
    },
    email: {
        type: String,
        required: true,
        min: 4
    },
    username: {
        type: String,
        required: true,
        min: 4
    },
    password: {
        type: String,
        required: true,
        min: 4
    },
    contact: {
        type: String,
        required: true,
        min: 4
    },
    skypeName: {
        type: String,
        required: true,
        min: 4
    },

});
module.exports = mongoose.model('Parents', parentSchema);