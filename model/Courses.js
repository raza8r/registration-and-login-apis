const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const courseTypeSchema = mongoose.Schema({
    name:{
        type: String,
        required: true
    }
});

const courseSchema = mongoose.Schema({

    name: {
        type: String,
        required: true,
        min: 4
    },
    course: [{
        type: Schema.Types.ObjectId,
        ref: 'CoursesType',
    }],
    teacher: [{
        type: Schema.Types.ObjectId,
        ref: 'teachers',
        required: true
    }],
    material: {
        type: Schema.Types.ObjectId,
        ref: 'CourseMaterial'
    }
});


module.exports = mongoose.model('CoursesType', courseTypeSchema);
module.exports = mongoose.model('Courses', courseSchema);