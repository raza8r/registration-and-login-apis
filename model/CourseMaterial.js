const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const materialSchema = mongoose.Schema({
    lesson: {
        type: String,
        required: true
    },
    course: [{
        type: Schema.Types.ObjectId,
        ref: 'Courses'
    }],
    courseFile: {
        type: String,
        required: true
    }
});

module.exports = mongoose.model('CourseMaterial', materialSchema);