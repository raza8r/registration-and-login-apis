const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const studentSchema = mongoose.Schema({
    name: {
        type: String,
        required: true
    },
    date: {
        type: Date,
        default: Date.now,
    },
    classes: {
        type: String,
        enum: ['Regular Classes', 'Trial Classes'],
        default: 'Rugular Classes'
    },
    city: {
        type: String,
        required: true
    },
    state: {
        type: String,
        required: true
    },
    gender: {
        type: String,
        enum: ['Male', 'Female'],
        default: 'Male'
    },
    age: {
        type: String,
        required: true
    },
    parent: [{
        type: Schema.Types.ObjectId,
        ref: 'parents'
    }],
  
    username: {
        type: String,
        required: true
    },
    password: {
        type: String,
        required: true
    },
});

module.exports = mongoose.model('Students', studentSchema);
