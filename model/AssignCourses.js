const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const assignSchema = mongoose.Schema({
    teacher: [{
        type: Schema.Types.ObjectId,
        ref: 'teachers',
        required: true
    }],
    course: [{
        type: Schema.Types.ObjectId,
        ref: 'courses',
        required: true
    }]
});

module.exports = mongoose.model('AssignCourses', assignSchema)