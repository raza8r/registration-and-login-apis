const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const teacherSchema = mongoose.Schema({

    name: {
        type: String,
        required: true,
        min: 4
    },
    cnic: {
        type: String,
        required: true,
        min: 4
    },
    gender: {
        type: String,
        enum: ['Male', 'Female'],
        default: 'Male'
    },
    degree: {
        type: String,
        required: true,
        min: 4
    },
    salaryType: {
        type: String,
        enum: ['Fixed Salary', 'Per Class Payment'],
        default: 'Fixed Salary'
    },
    salary: {
        type: String,
        required: true,
        min: 4
    },
    username: {
        type: String,
        required: true,
        min: 4
    },
    password: {
        type: String,
        // required: true,
    },
    selectedStudent:[{
        type: Schema.Types.ObjectId,
        ref: 'Students'
    }]

});

module.exports = mongoose.model('Teachers', teacherSchema);