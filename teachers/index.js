const router = require('express').Router();
const Teachers = require('../model/Teachers');
const { teacherValidation } = require('../validation');
const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');



//Register New Teacher
router.post('/teachers', async (req, res) => {

    //Validate teachers
    const { error } = teacherValidation(req.body);
    if (error) return res.status(401).json({ error: error.details[0].message });

    //username and cnic already exist
    const userExist = await Teachers.findOne({ username: req.body.username });
    if (userExist) return res.status(401).json({ error: "User is already exist" });
    const cnicExist = await Teachers.findOne({ cnic: req.body.cnic });
    if (cnicExist) return res.status(401).json({ error: "CNIC is already exist" });

    //Hash Password
    const salt = await bcrypt.genSalt(10);
    const hashedPassword = await bcrypt.hash(req.body.password, salt);


    try {
        const teacher = new Teachers({
            name: req.body.name,
            cnic: req.body.cnic,
            gender: req.body.gender,
            degree: req.body.degree,
            salaryType: req.body.salaryType,
            salary: req.body.salary,
            username: req.body.username,
            password: hashedPassword,
            selectedCourses: req.body.selectedCourses
        });
        const savedTeacher = await teacher.save();
        //pass single id not complete data
        // res.status(200).json({teacher: teacher._id});

        res.status(200).json({ savedTeacher });
    }
    catch (err) {
        res.status(400).json({ message: err });
    }
});

//Login Teacher
router.post('/teacherlogin', async (req, res) => {

    //Validation
    // const { error } = await teacherValidation(req.body);
    // if (error) return res.status(400).json({ error: error.details[0].message });

    //Check if the username exist
    const teacher = await Teachers.findOne({ username: req.body.username });
    if (!teacher) return res.status(400).json({ error: "Username is not exist" });

    //compare the password
    const loginPassword = await bcrypt.compare(req.body.password, teacher.password);
    if (!loginPassword) return res.status(400).json({ error: "Invalid Password" });

    //create and assign token
    const token = jwt.sign({ _id: teacher._id }, process.env.TOKEN_SECRET);
    res.header('auth-token', token).send(token);

    // res.json("Login");
});

//Get Teachers Record
router.get('/teachers', async (req, res) => {
    try {
        const teacherRecord = await Teachers.find();
        res.status(200).json(teacherRecord);
    }
    catch (err) {
        res.json({ message: err });
    }
});

//Search Specific Record
router.get('/:teachersId', async (req, res) => {
    try {
        const teacher = await Teachers.findById(req.params.teachersId);
        res.json(teacher);
    } catch (err) {
        res.json({ message: err });
    }
});

//Update Teachers Record
router.patch('/:teachersId', async (req, res) => {
    try {
        const teacherUpdate = await Teachers.updateOne({ _id: req.params.teachersId }, {
            $set: {
                name: req.body.name,
                cnic: req.body.cnic,
                gender: req.body.gender,
                degree: req.body.degree,
                salaryType: req.body.salaryType,
                salary: req.body.salary,
                username: req.body.username,
                selectedStudent: req.body.selectedStudent
                // password: hashedPassword
            }
        });
        res.status(200).json(teacherUpdate)
    }
    catch (err) {
        res.status(400).json({ message: err });
    }
});

//Delete Teacher
router.delete('/:teachersId', async (req, res) => {
    try {
        const deleteTeacher = await Teachers.remove({ _id: req.params.teachersId });
        res.status(200).json(deleteTeacher);

    } catch (err) {
        res.status(400).json({ message: err });
    }
});

module.exports = router;