
const router = require('express').Router();
const Courses = require('../model/Courses');

//Register New Parents
router.post('/courses', async (req, res) => {
    try {
        const course = new Courses({
            name: req.body.name,
            course: req.body.course,
            teacher: req.body.teacher,
            material: req.body.material
        });
        const savedCourse = await course.save();
        res.status(200).json({ savedCourse });
    }
    catch (err) {
        res.status(400).json({ message: err });
    }
});

//Get parents Record

router.get('/courses', async (req, res) => {
    try {
        const course = await Courses.find();
        res.status(200).json(course);
    }
    catch (err) {
        res.json({ message: err });
    }
});

//Search Specific Record
router.get('/:courseId', async (req, res) => {
    try {
        const course = await Courses.findById(req.params.courseId);
        res.json(course);
    } catch (err) {
        res.json({ message: err });
    }
});

//Update parents Record
router.patch('/:courseId', async (req, res) => {
    try {
        const course = await Courses.updateOne({ _id: req.params.courseId }, {
            $set: {
                name: req.body.name,
                course: req.body.course,
                teacher: req.body.teacher
            }
        });
        res.status(200).json(course)
    }
    catch (err) {
        res.status(400).json({ message: err });
    }
});

//Delete Teacher
router.delete('/:courseId', async (req, res) => {
    try {
        const course = await Courses.remove({ _id: req.params.courseId });
        res.status(200).json(course);

    } catch (err) {
        res.status(400).json({ message: err });
    }
});

module.exports = router;