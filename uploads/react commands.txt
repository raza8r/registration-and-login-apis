npx create-react-app appname  //for create new react app
npm start  //for server
npm install react-router-dom  // to create react router

npm install redux
npm install react-redux
npm install redux-saga

reactstrap
----------------------------------
npm install --save bootstrap
npm install --save reactstrap react react-dom

--\
import 'bootstrap/dist/css/bootstrap.css';
---------------------------------------------
npm install --save react-router
............................................
import { BrowserRouter as Router, Route, Switch } from "react-router-dom"; //use in Menu Page
---------------------------------------------
const NewRoutes = props => (
  <>
    {SecondEditorRoute && SecondEditorRoute.map(route => {
      return <Row>
                <div className="eMargin">
                  <div>
                    <img src={route.imgage} alt="" className="imgEditorSecond" />
                  </div>
                  <div style={{ marginLeft: "20px" }}>
                  <h6>{route.discription}</h6>
                      <p><b>{route.title}</b> in <b>{route.in}</b></p>
                      <p style={{color:'lightgray'}}>{route.date}</p>
                  </div>
                </div>
              </Row>
    })}

  </>
)



---------------------------
import React, { Component } from "react";
import "./SecondryHeader.css";
import {
  Collapse,
  Navbar,
  NavbarToggler,
  Nav,
  NavItem,
  NavLink
} from "reactstrap";
import NavRoutes from "../../../Routes/NavRoutes";

class SecondryHeader extends Component {
  render() {
    return (
      <div className="margin">
        <Navbar light expand="md">
          <NavbarToggler />
          <Collapse navbar>
            <Nav className="mr-auto, padding" navbar>
              <Routes />
            </Nav>
          </Collapse>
        </Navbar>
      </div>
    );
  }
}

const Routes = props => (
  <>
    {NavRoutes &&
      NavRoutes.map(route => {
        return (
          <NavItem>
            <NavLink href={route.href}>{route.name}</NavLink>
          </NavItem>
        );
      })}
  </>
);

export default SecondryHeader;
------------------------------------------------------------------
let navbarPost = [
    {
      name: "Home",
      href: "/"
    },
    {
        name: "Events",
        href: "/"
    },
    {
        name: "Apply",
        href: "/applyform"
    },
    {
        name: "Applocation",
        href: "/applications"
    },
    {
        name: "Login",
        href: "/login"
    },
    {
        name: "Signup",
        href: "/signup"
    },
    {
        name: "Discover",
        href: "/discover"
    },
    {
        name: "Add Project",
        href: "/addproject"
    },
    {
        name: "Project",
        href: "/projects"
    },
    
]

export default navbarPost

----------------///////// Refactoring And Reuseable Functional Components///--------------------

const PrimaryBrand = (props:{name:string,image:string,description:string}) =>  {
    let {name, image, description} = props
    return (
        <Col md="4" lg="4" sm="4" xs="12">
            <Card body id="primaryCardStyle">
                <img
                    src={image}
                    alt="Image"
                    id="primaryBrandImage"
                />
                    <h5 id="primaryBrandHead">{name}</h5>
                <CardText id="primaryCardText">
                    {description}
                </CardText>
            </Card>
        </Col>
    );
}
++++++++++++++++++++++++++++++++++++++Use In Main Component--- Data Pass through props++++++++++++++++++++
	<Row id="rowCardBrand">
          {Brands && Brands.map(brand=>{
            return(
            <PrimaryBrand 
              image={brand.image}
              name={brand.name}
              description={brand.description}
            />
            )
          })}
          </Row>
